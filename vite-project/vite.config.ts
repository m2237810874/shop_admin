import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve:{
    alias:{
      '@':path.resolve(__dirname,'./src')
    }
  },
  server:{
    port:8080,
    proxy:{//配置跨域
      '/api':{
        target:'https://bjwz.bwie.com/mall4w',//接口域名
        changeOrigin:true,//是否跨域
        rewrite:path=>path.replace(/^\/api/,''),
      }
    }
  }
})
