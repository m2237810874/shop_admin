// TS类型
export interface ResponseData<T>{
    status:number,
    statusText:string,
    data:T
}

export interface LoginResponseData{
    access_token:string,
    token_type:string,
    refresh_token:string,
    authorities:any[],
    shopId:number,
    expires_in:number,
    userId:number
}