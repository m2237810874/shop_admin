const ProduList=[
    {
        path:'/group',
        name:'prod:prodTag',
        component:()=>import('../../views/home/product/group/index.vue')
    },
    {
        path:'/produce',
        name:'prod:produce',
        component:()=>import('../../views/home/product/produce/index.vue')
    }
];
export default ProduList;
