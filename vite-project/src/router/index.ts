import { RouteRecordRaw, createRouter, createWebHistory } from 'vue-router';
const routes: RouteRecordRaw[] = [
    {
        path:'/',
        redirect:'/homepage'
    },
    {
        path: '/home',
        name: 'home',
        component: () => import('../views/home/index.vue'),
        meta: { requiresAuth: true },
        children: [
            {
                path: '/homepage',
                name: 'homepage',
                component: () => import('../views/home/homePage/index.vue'),
            },
            {
                path: '/sys/area',
                name: 'address',
                component: () => import('../views/home/systemmanagement/address/index.vue'),
            },
            {
                path: '/user/user',
                name: 'members',
                component: () => import('../views/home/members/index.vue'),
            },
            {
                path: '/sys/user',
                name: 'administrator',
                component: () => import('../views/home/systemmanagement/administrator/index.vue'),
            },
            {
                path: '/sys/menu',
                name: ' menu',
                component: () => import('../views/home/systemmanagement/menu/index.vue'),
            },
            {
                path: '/sys/config',
                name: 'parameter',
                component: () => import('../views/home/systemmanagement/parameter/index.vue'),
            },
            {
                path: '/sys/role',
                name: 'role',
                component: () => import('../views/home/systemmanagement/role/index.vue'),
            },
            {
                path: '/sys/log',
                name: 'sytem',
                component: () => import('../views/home/systemmanagement/system/index.vue'),
            },
            {
                path: '/sys/schedule',
                name: 'timing',
                component: () => import('../views/home/systemmanagement/timing/index.vue'),
            },
            {
                path: '/shop/notice',
                name: 'notice',
                component: () => import('../views/home/storeManagement/notice/index.vue'),
            },
            {
                path: '/shop/hotSearch',
                name: 'toSearch',
                component: () => import('../views/home/storeManagement/toSearch/index.vue'),
            },
            {
                path: '/admin/indexImg',
                name: 'swiper',
                component: () => import('../views/home/storeManagement/swiper/index.vue'),
            },
            {
                path: '/shop/transport',
                name: 'feight',
                component: () => import('../views/home/storeManagement/feight/index.vue'),
            },
            {
                path: '/shop/pickAddr',
                name: 'pickUpPoints',
                component: () => import('../views/home/storeManagement/pickUpPoints/index.vue'),
            },
            {
                path: '/prod/prodTag',
                name: 'group',
                component: () => import('../views/home/product/group/index.vue')
            },
            {
                path: '/prod/prodList',
                name: 'produce',
                component: () => import('../views/home/product/produce/index.vue')
            },
            {
                path: '/prod/category',
                name: 'classify',
                component: () => import('../views/home/product/classify/index.vue')
            },
            {
                path: '/prod/prodComm',
                name: 'comment',
                component: () => import('../views/home/product/comment/index.vue')
            },
            {
                path: '/prod/spec',
                name: 'sku',
                component: () => import('../views/home/product/sku/index.vue')
            },
            {
                path: '/prod/prodInfo',
                name: 'prodInfo',
                component:()=>import('../views/home/product/prodInfo/prodInfo.vue')
            },
            {
                path: '/order/order',
                name: 'orderOrder',
                component:()=>import('../views/home/order/index.vue')
            }
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('../views/login/index.vue')
    },
    {
        path: '/:pathMatch(.*)',
        component: () => import('../views/404/index.vue')
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes
})

router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('token')
    if (to.meta.requiresAuth && !token) {
        next({
            name: 'login'
        })
    } else {
        next()
    }
})


export default router