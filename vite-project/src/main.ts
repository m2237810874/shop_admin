import { createApp } from 'vue';
import './style.css';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import App from './App.vue';
import router from './router';
import {createPinia} from 'pinia'
import Avue from '@smallwei/avue';
import '../node_modules/@smallwei/avue/lib/index.css';
import locale from "element-plus/lib/locale/lang/zh-cn"



const app = createApp(App);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
}



createApp(App).mount('#app');
app.use(router)
app.use(Avue);
app.use(createPinia())
app.use(ElementPlus,{locale});
app.mount('#app');
