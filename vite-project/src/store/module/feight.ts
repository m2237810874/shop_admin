import {defineStore} from 'pinia'
import {feightList,deleteone,delete_many} from "../../api/feight"
const feightStore:any=defineStore('feight',{
    state(){
        return{
           data:[],
           dialogVisible:false,
           delId:null,
           current:1,
           size:10,
           feightpage:{},
           proSearch:{

           },
           addfeightDisplay:false,
           feightForm:{
            chargeType: 0,
            hasFreeCondition: 0,
            isFreeFee: 0,
            transName: "",
            transfeeFrees: [{freeCityList: [], freeType: 0}],
            transfees: [{cityList: [], status: 1, firstPiece: 1, firstFee: "0", continuousPiece: 1, continuousFee: "1"}],
           },
           transportId:[],
        }
    },
    actions:{
        async getList(){
            const res:any=await feightList({
              t:1667273730008,
              current:this.current,
              size:this.size
            })
            this.data=res.data.records
              //分页
              this.feightpage=res.data
          },
          async truedelone(){
            await deleteone([this.delId])
            this.getList()
        },
         //批量删除 
         async delets() {
          const id: any = []
          this.transportId.forEach((item: any) => {
              id.push(item.transportId)
          })
          const res: any = await delete_many(id)
          if (res.status === 200) {
              this.getList()
          }
      },
      feightChange(value: any) {
          this.transportId = value

      },
        async searchnoList(row:any){
          const res:any=await feightList({
              current:this.current,
              size:this.size,
              transName:row.transName,
          })
          this.data=res.data.records
      },
       // 分页
       feightSizeChange(page:any){
        this.current=1
        this.size=page
        this.getList()

    },
    feightCurrentChange(page:any){
        this.current=page
        this.getList()
    }
    }
})
export default feightStore