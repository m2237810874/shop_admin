import { defineStore } from 'pinia'
import { getRoutes } from '../../utils/permission'
import {_login,_get_menulist,_get_userinfo , changePwd } from '../../api/login'
import cache from '../../utils/util'
import router from '../../router'
import { ElMessage } from 'element-plus'
const login = defineStore('login',{
    state(){
        return{
            token:cache.getCache('token')||'',
            authorities:cache.getCache('authorities')||[],
            userInfo:cache.getCache('userinfo')||{},
            menuList:cache.getCache('menuList')||[],
            LogindialogVisible:false,
            originalInput:'',
            NewPwd:'',
            TwoNewPwd:''
        }
    },
    getters: {

    },
    actions: {
        async loginAction(){
            //获取用户信息
            const userInfo:any=await _get_userinfo();
            this.userInfo=userInfo.data;
            cache.setCache('userinfo',userInfo.data);
            //获取导航列表
            const menuList:any=await _get_menulist();
            this.menuList=menuList.data.menuList;
            cache.setCache('menuList',menuList.data.menuList);
            this.changeMenu(menuList.data.menuList)
        },
        changeMenu(payload:any){
            const routes=getRoutes(payload)
            //动态添加到路由表里面
            //addRoutes addRoute
            //addRoute 路由中的api 是再让你国家的一级路由 如果要增加二级路由一定要带上父路由地址
            routes.forEach(item=>{
                router.addRoute('home',item);
                
            })
        },
        getMenu(){
            //防止杀心的时候丢失menu数据
            //刷新的时候 重新获取menu从本地
            const menuList=cache.getCache('menuList');
            const authorities=cache.getCache('authorities')
        },
        //修改密码
        async ChangePwd(){
            this.LogindialogVisible=true
            if(this.NewPwd!==this.TwoNewPwd){
                ElMessage.error('新密码两次输入不一致')
            }else{
                await changePwd({
                    password:this.originalInput,
                    newPassword:this.NewPwd
                })
                this.LogindialogVisible=false
                router.push('/login')
            }
        }
    }
})

export default login