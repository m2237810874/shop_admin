import { defineStore } from 'pinia'
import { get_skuList, edit_skuList, add_skuList } from '../../api/sku'
import { get_groupList, add_groupList, edit_groupList } from '../../api/group'
import { get_classList, add_classList, edit_classList } from '../../api/class'
import { treeDataTranslate } from "../../utils/tool"
const productStore: any = defineStore('product', {
    state() {
        return {
            dataListLoading: false,
            // 分组管理
            columnValue: [] as any,
            groupList: [],
            groupCurrent: 1,
            groupSize: 10,
            groupDetail: {},
            groupSearch: {},
            addgroupDisplay: false,
            columnDisplay: false,
            groupForm: {
                id: null,
                title: null,
                shopId: null,
                status: 1,
                isDefault: null,
                prodCount: null,
                seq: null,
                style: 0
            } as any,
            editgroupForm: {
                id: null,
                title: null,
                shopId: null,
                status: 1,
                isDefault: null,
                prodCount: null,
                seq: null,
                style: 0,
            },
            editgroupDisplay: false,
            option: {
                search: {},
                border: true,
                align: 'center',
                addBtn: false,
                menuWidth: 350,
                searchBtn: false,
                emptyBtn: false,
                refreshBtn: true,
                stripe: true,
                indexLabel: '序号',
                indexWidth: 60,
                index: true,
                dialogDrag: true,
                dialogEscape: true,
                editTitle: true,
                editBtn: false,
                delBtn: false,
                searchMenuSpan: 3,
                columnBtn: false,
                column: [{
                    label: '标签名称',
                    prop: 'title',
                    search: true,
                    key: 1,
                    hide: false
                }, {
                    label: '状态',
                    prop: 'status',
                    solt: true,
                    search: true,
                    key: 2,
                    hide: false
                }, {
                    label: '默认类型',
                    prop: 'isDefault',
                    solt: true,
                    key: 3,
                    hide: false
                }, {
                    label: '排序',
                    prop: 'seq',
                    key: 4,
                    hide: false
                }
                ]
            } as any,
            // 分类管理
            addclassDisplay: false,
            editclassDisplay: false,

            classList: [],
            commentList: [],
            addclassForm: {
                categoryName: '',
                status: '0',
                seq: '',
                pic: null,
                categoryId: '',
                grade: 0
            },
            editclassForm: {
                categoryName: '',
                status: '0',
                seq: '',
                pic: null,
                categoryId: '',
                grade: 0
            },
            categoryTreeProps: {
                value: 'categoryId',
                label: 'categoryName'
            },
            // sku规格
            skuList: [],
            skuCurrent: 1,
            skuSize: 10,
            skuDetail: {},
            skuSearch: {},
            skuUser: {},
            skuDisplay: false,
            addskuDisplay: false,
            skucolumnDisplay: false,
            skucolumnValue: [] as any,
            skuDisplayList: [{ propId: 0, propName: '', prodPropValues: [] }] as any,
            addskuDisplayList: [{ propId: 0, propName: '', prodPropValues: [{}] }] as any,
            skuoption: {
                border: true,
                align: 'center',
                addBtn: false,
                menuWidth: 350,
                searchBtn: false,
                emptyBtn: false,
                indexLabel: '序号',
                index: true,
                indexWidth: 60,
                editBtn: false,
                delBtn: false,
                searchMenuSpan: 3,
                columnBtn: false,
                editTitle: '修改',
                column: [{
                    label: '属性名称',
                    prop: 'propName',
                    search: true,
                    key: 1,
                    hide: false
                }, {
                    label: '属性值',
                    prop: 'prodPropValues',
                    key: 2,
                    hide: false
                },
                ]
            }
        }
    },
    actions: {
        // 分组管理
        // 分组获取数据
        async get_groupList() {
            const res: any = await get_groupList({
                current: this.groupCurrent,
                size: this.groupSize
            })
            this.groupDetail = res.data
            this.groupList = res.data.records
        },
        // 分组分页
        groupCurrentChange(page: number) {
            this.groupCurrent = page
            this.get_groupList()
        },
        groupSizeChange(page: number) {
            this.groupCurrent = 1
            this.groupSize = page
            this.get_groupList()
        },
        // 分组搜索
        async searchgroupList(row: any) {
            const res: any = await get_groupList({
                current: this.groupCurrent,
                size: this.groupSize,
                title: row.title ? row.title : null,
                status: row.status ? row.status === '正常' ? 1 : 0 : null
            })
            this.groupDetail = res.data
            this.groupList = res.data.records
        },
        // 分组清空
        cleargroupSearch() {
            this.groupSearch = {}
            this.get_groupList()
        },
        // 分组新增
        async addgroupList() {
            const res: any = await add_groupList({
                id: 0,
                isDefault: 0,
                prodCount: 0,
                seq: parseInt(this.groupForm.seq),
                shopId: null,
                status: this.groupForm.status,
                style: this.groupForm.style,
                title: this.groupForm.title,
            })
            if (res.status === 200) {
                this.addgroupDisplay = false
                this.get_groupList()
            }

        },
        groupDisplay(row: any) {
            this.editgroupDisplay = true
            this.editgroupForm = row
        },
        async editgroupList() {
            const res: any = await edit_groupList(this.editgroupForm)
            this.editgroupDisplay = false
            if (res.status === 200) {
                this.get_groupList()
            }
        },
        // 分组显隐
        columnDisplayTrue() {
            this.columnDisplay = true
            this.columnValue = []
            this.option.column.forEach((item: any) => {
                if (item.hide === false) {
                    this.columnValue.push(item.key)
                }
            })
        },
        handleChange(value: any, direction: any, movedKeys: any) {
            if (direction === 'left') {
                this.option.column.forEach((item: any) => {
                    movedKeys.forEach((v: any) => {
                        if (item.key === v) {
                            item.hide = true
                        }
                    })
                })
            }
            else {
                this.option.column.forEach((item: any) => {
                    movedKeys.forEach((v: any) => {
                        if (item.key === v) {
                            item.hide = false
                        }
                    })
                })
            }
        },
        // 分类管理
        async get_classList() {
            const res: any = await get_classList()
            this.classList = treeDataTranslate(res.data, 'categoryId', 'parentId')
            console.log(this.classList);

        },
        classDisplay() {
            console.log(123);
            this.addclassDisplay = true
        },
        async addclassList() {
            const res: any = await add_classList(this.addclassForm)
            if (res.status === 200) {
                this.get_classList()
                this.addclassDisplay = false
            }
        },
        editclassDisplayTrue(row: any) {
            this.editclassForm = row
            this.editclassDisplay = true
        },
        async editclassList() {
            const res: any = await edit_classList(this.editclassForm)
            if (res.status === 200) {
                this.get_classList()
                this.editclassDisplay = false
            }
        },
        // sku规格
        // sku搜索
        async searchSkuList(row: any) {
            const res: any = await get_skuList({
                current: this.skuCurrent,
                size: this.skuSize,
                propName: row.propName
            })
            this.skuDetail = res.data
            this.skuList = res.data.records
            this.skuCurrent = 1
        },
        // sku获取数据
        async get_List() {
            const res: any = await get_skuList({
                current: this.skuCurrent,
                size: this.skuSize
            })
            this.skuDetail = res.data
            this.skuList = res.data.records
        },
        // sku分页
        skuCurrentChange(page: number) {
            this.skuCurrent = page
            this.get_List()
        },
        skuSizeChange(page: number) {
            this.skuCurrent = 1
            this.skuSize = page
            this.get_List()
        },
        // sku清空
        clearPropName(row: any) {
            this.skuSearch = {}
            this.get_List()
        },
        // sku修改弹框
        editDisplay(row: any) {
            console.log(row);
            this.skuDisplay = true
            this.skuDisplayList[0].propId = row.propId
            this.skuDisplayList[0].propName = row.propName
            this.skuDisplayList[0].prodPropValues = row.prodPropValues
        },
        // sku修改
        async editskuList(value: any) {
            const res: any = await edit_skuList(value[0])
            if (res.status === 200) {
                this.get_List()
                this.skuDisplay = false
            }
        },
        // sku添加弹框
        addDisplay() {
            this.addskuDisplay = true
        },
        // sku添加
        addskuList(value: any) {
            add_skuList(value[0])
            this.addskuDisplay = false
            this.get_List()
        },
        // sku添加input框
        addInput(value: any) {
            if (value[0].prodPropValues[value[0].prodPropValues.length - 1].propValue) {
                this.addskuDisplayList[0].prodPropValues.push({})
            }
        },
        editInput(value: any) {
            if (value[0].prodPropValues[value[0].prodPropValues.length - 1].propValue) {
                this.skuDisplayList[0].prodPropValues.push({})
            }
        },
        skucolumnDisplayTrue() {
            this.skucolumnDisplay = true
            this.skucolumnValue = []
            this.skuoption.column.forEach((item: any) => {
                if (!item.hide) {
                    this.skucolumnValue.push(item.key)
                }
            })
        },
        skuhandleChange(value: any, direction: any, movedKeys: any) {
            if (direction === 'left') {
                this.skuoption.column.forEach((item: any) => {
                    movedKeys.forEach((v: any) => {
                        if (item.key === v) {
                            item.hide = true
                        }
                    })
                })
            }
            else {
                this.skuoption.column.forEach((item: any) => {
                    movedKeys.forEach((v: any) => {
                        if (item.key === v) {
                            item.hide = false
                        }
                    })
                })
            }
        }
    }
})
export default productStore