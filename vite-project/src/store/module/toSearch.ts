
import { defineStore } from 'pinia'
import { toSearchList, delete_one, add_toSearchList, edit_toSearchList, delete_many } from "../../api/toSearch"
const toSearchStore: any = defineStore('toSearch', {
    state() {
        return {
            data: [],
            dialogVisible: false,
            delId: null,
            //分页
            current: 1,
            size: 10,
            toSearchpage:{},
            proSearch: {

            },
            //新增
            toSearchDisplay: false,
            toSearchForm: {
                content: "",
                hotSearchId: 0,
                recDate: "",
                seq: 0,
                status: 0,
                title: "",
            } as any,
            //修改
            edittoSearchDisplay: false,
            edittoSearchForm: {
                content: "",
                hotSearchId: 0,
                recDate: "",
                seq: 0,
                shopId: 0,
                status: 0,
                title: "",
            },
            //批量删除
            hotSearchId: [],
        }
    },
    actions: {
        //渲染数据
        async getList() {
            const res: any = await toSearchList({
                t: 1666874913609,
                current: this.current,
                size: this.size
            })
            this.data = res.data.records
            //分页
            this.toSearchpage=res.data
            // console.log(this.toSearchpage);
            
        },
        //   删除
        async truedelone() {
            await delete_one([this.delId])
            this.getList()
        },
        //批量删除 
        async delets() {
            const id: any = []
            this.hotSearchId.forEach((item: any) => {
                id.push(item.hotSearchId)
            })
            const res: any = await delete_many(id)
            if (res.status === 200) {
                this.getList()
            }
        },
        toSearchChange(value: any) {
            this.hotSearchId = value

        },
        //搜索
        async searchnoList(row: any) {
            const res: any = await toSearchList({
                current: this.current,
                size: this.size,
                title: row.title ? row.title : null,
                content: row.content ? row.content : null,
                status: row.status ? row.status === '启用' ? 1 : 0 : null,

            })
            this.data = res.data.records
        },
        // 新增
        async addtoSearchList() {
            const res: any = await add_toSearchList(this.toSearchForm)
            if (res.status === 200) {
                this.toSearchDisplay = false
                this.getList()
                this.toSearchForm={}
            }
        },
        //修改
        editDisplay(row: any) {
            this.edittoSearchDisplay = true
            this.edittoSearchForm = row
        },
        async edittoSearchList() {
            // console.log(this.getList);
            const res: any = await edit_toSearchList(this.edittoSearchForm)
            this.edittoSearchDisplay = false
            if (res.status === 200) {
                this.getList()
            }
        },
        // 分页
        SearchSizeChange(page:any){
            this.current=1
            this.size=page
            this.getList()

        },
        searchCurrentChange(page:any){
            this.current=page
            this.getList()
        }
    }
})
export default toSearchStore