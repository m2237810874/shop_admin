import { defineStore } from 'pinia'
import { get_roleList, get_navList, del_roleList, add_roleList, get_editFormList, edit_roleList } from '../../api/role'
import { treeDataTranslate } from "../../utils/tool"
const roleStore: any = defineStore('roleStore', {
    state() {
        return {
            roleList: [],
            roleCurrent: 1,
            columnDisplay: false,
            roleoption: {
                search: {},
                border: true,
                align: 'center',
                addBtn: false,
                searchBtn: false,
                emptyBtn: false,
                selection: true,
                editBtn: false,
                delBtn: false,
                searchMenuSpan: 3,
                columnBtn: false,
                column: [{
                    label: '角色名称',
                    prop: 'roleName',
                    search: true,
                    key: 1,
                    hide: false
                }, {
                    label: '备注',
                    prop: 'remark',
                    key: 2,
                    hide: false
                }, {
                    label: '创建时间',
                    prop: 'createTime',
                    key: 3,
                    hide: false
                }
                ]
            },
            roleSize: 10,
            roleDetail: {},
            roleSearch: {},
            addroleDisplay: false,
            editroleDisplay: false,
            roleForm: {
                menuIdList: [],
                remark: "",
                roleName: ""
            },
            editroleForm: {
                menuIdList: [],
                remark: "",
                roleName: ""
            } as any,
            navList: [],
            columnValue: [] as any,
            menuListId: [] as any,
            addroleListAll: [],
            FormList: {},
            ListId: [],
            roleId: 0,
        }
    },
    actions: {
        async get_roleList() {
            const res: any = await get_roleList({
                current: this.roleCurrent,
                size: this.roleSize
            })
            this.roleList = res.data.records
            this.roleDetail = res.data
        },
        // 角色分页
        roleCurrentChange(page: number) {
            this.roleCurrent = page
            this.get_roleList()
        },
        roleSizeChange(page: number) {
            this.roleCurrent = 1
            this.roleSize = page
            this.get_roleList()
        },
        // 角色搜索
        async searchroleList(row: any) {
            const res: any = await get_roleList({
                current: this.roleCurrent,
                size: this.roleSize,
                roleName: row.roleName
            })
            if (res.status === 200) {
                this.roleList = res.data.records
                this.roleDetail = res.data
                this.roleCurrent = 1
            }
        },
        // 角色清空
        clearroleSearch() {
            this.roleSearch = {}
            this.get_roleList()
        },
        // 新增
        addroleDisplayTrue() {
            this.addroleDisplay = true
            this.get_navList()
        },
        async get_navList() {
            const res: any = await get_navList()
            this.navList = treeDataTranslate(res.data, 'menuId', 'parentId')
        },
        async addroleList() {
            const res: any = await add_roleList(this.roleForm)
            if (res.status === 200) {
                this.get_roleList()
            }
            this.addroleDisplay = false
        },
        handleCheckChange(data: any, checked: boolean, indeterminate: boolean) {
            this.menuListId = []
            if (checked) {
                this.menuListId.push(data.menuId)
                this.for(data);
            }
            this.roleForm.menuIdList = this.menuListId
        },
        for(data: any) {
            if (data.children) {
                data.children.forEach((item: any) => {
                    this.menuListId.push(item.menuId)
                })
            }
        },
        // 修改
        async editroleDisplayTrue(row: any) {
            this.roleId = row.roleId
            this.editroleDisplay = true
            this.get_navList()
            const res: any = await get_editFormList(row.roleId)
            this.FormList = res.data
            this.editroleForm = this.FormList
        },
        async editroleList() {
            // this.editroleForm.menuIdList=this.ListId
            const res: any = await edit_roleList({
                menuIdList: this.ListId,
                remark: this.editroleForm.remark,
                roleName: this.editroleForm.roleName,
                roleId: this.roleId
            })
            if (res.status === 200) {
                this.get_roleList()
            }
            this.editroleDisplay = false
        },
        // 批量删除
        selectionChange(val: any) {
            this.addroleListAll = val
        },
        async delAllroleList() {
            const roleListId: any = []
            this.addroleListAll.forEach((item: any) => {
                roleListId.push(item.roleId)
            })
            const res: any = await del_roleList(roleListId)
            if (res.status === 200) {
                this.get_roleList()
            }
        },
        columnDisplayTrue() {
            this.columnDisplay = true
            this.columnValue = []
            this.roleoption.column.forEach((item: any) => {
                if (item.hide === false) {
                    this.columnValue.push(item.key)
                }
            })
        },
        handleChange(value: any, direction: any, movedKeys: any) {
            if (direction === 'left') {
                this.roleoption.column.forEach((item: any) => {
                    movedKeys.forEach((v: any) => {
                        if (item.key === v) {
                            item.hide = true
                        }
                    })
                })
            }
            else {
                this.roleoption.column.forEach((item: any) => {
                    movedKeys.forEach((v: any) => {
                        if (item.key === v) {
                            item.hide = false
                            console.log(item.hide);

                        }
                    })
                })
            }
        },
    }
})
export default roleStore