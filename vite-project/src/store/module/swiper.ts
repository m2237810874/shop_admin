
import { defineStore } from 'pinia'
import { swiperList, deleteone, add_swiperList, edit_swiperList, delete_many } from "../../api/swiper"
const swiperStore: any = defineStore('swiperStore', {
    state() {
        return {
            data: [],
            dialogVisible: false,
            delId: null,
            current: 1,
            size: 10,
            swiperpage:{},
            proSearch: {

            },
            addSwiperDisplay: false,
            swiperForm: {
                des: "",
                imgId: 0,
                imgUrl: "",
                relation: null,
                seq: 0,
                status: 0,
                type: -1,
            }as any,

            //编辑
            editSwiperDisplay: false,
            editswiperForm: {
                des: "",
                imgId: 0,
                imgUrl: "",
                relation: null,
                seq: 0,
                status: 0,
                type: 0
            },
            //批量删除
            imgId: [],
        }
    },
    actions: {
        async getList() {
            const res: any = await swiperList({
                t: 1666924328049,
                current: this.current,
                size: this.size
            })
            this.data = res.data.records
            //分页
            this.swiperpage=res.data
        },
        //   删除
        async truedelone() {
            await deleteone([this.delId])
            this.getList()
        },
        //批量删除 
        async delets() {
            const id: any = []
            this.imgId.forEach((item: any) => {
                id.push(item.imgId)
            })
            const res: any = await delete_many(id)
            if (res.status === 200) {
                this.getList()
            }
        },
        swiperChange(value: any) {
            this.imgId = value

        },
        //搜索
        async searchnoList(row: any) {
            const res: any = await swiperList({
                current: this.current,
                size: this.size,
                status: row.status === "禁用" ? 0 : 1,

            })
            this.data = res.data.records
        },
        // 新增
        async addSwiperList() {
            const res: any = await add_swiperList(this.swiperForm)
            if (res.status === 200) {
                this.addSwiperDisplay = false
                this.getList()
                this.swiperForm={}
            }
           
        },
        //修改
        editswiper(row: any) {
            this.editSwiperDisplay = true
            this.editswiperForm = row

        },
        async editSwiperList() {
            const res: any = await edit_swiperList(this.editswiperForm)
            this.editSwiperDisplay = false
            if (res.status === 200) {
                this.getList()
            }
        },
        // 分页
        swiperSizeChange(page:any){
            this.current=1
            this.size=page
            this.getList()

        },
        swiperCurrentChange(page:any){
            this.current=page
            this.getList()
        }
    }
})
export default swiperStore