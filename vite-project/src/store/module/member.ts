// 会员管理
import { request } from "../../utils";
import { tableType } from "../../types";
interface getMemberReqType extends tableType {
  nickName?: string;
  status?: number;
}
interface editMemberType {
  nickName: string;
  status: number;
  userId: string;
}
export const getMember = (params: getMemberReqType) =>
  request.get({
    url: "/api/admin/user/page",
    method: "get",
    params,
  });
  export const getMemberInfo = (userId: string) =>
  request.get({
    url: `/api/admin/user/info/${userId}`,
    method: "get",
  });
export const editMember = (data: editMemberType) =>
  request.put({
    url: "/api/admin/user",
    method: "put",
    data,
  });
