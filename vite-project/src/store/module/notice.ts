import {defineStore} from 'pinia'
import {deleteone,get_noticeList,add_noticeList,edit_noticeList} from "../../api/notice"
const noticeStore:any=defineStore('notice',{
    state(){
        return{
           data:[],
           dialogVisible:false,
           delId:null,
           current:1,
           size:10,
           noticepage:{},
           proSearch:{

           },
           //新增
           noticeForm:{
            content: null,
            id: 0,
            isTop: 0,
            status: 0,
            title: "",
            url: null,
           } as any,
           addnoticeDisplay:false, //弹框
        //    编辑
            editnoticeDisplay:false,
            editnoticeForm:{
                content: null,
                id: 0,
                isTop: 0,
                publishTime: "",
                shopId: 0,
                status: 0,
                t: 0,
                title: "",
                updateTime: "",
            }
            }

    },
    actions:{
        async getList(){
            const res:any=await get_noticeList({
              t:1667292244294,
              current:this.current,
              size:this.size
            })
            this.data=res.data.records
            this.noticepage=res.data
            
          
        },
        //   删除
        async truedelone(){
            await deleteone([this.delId])
            this.getList()
        },
         //搜索
        async searchnoList(row:any){
            console.log(row);
          
            const res:any=await get_noticeList({
                current:this.current,
                size:this.size,
                title:row.title?row.title:null,
                status:row.status?row.status==='发布'?1:0:null,
                isTop:row.isTop?row.isTop==='是'?1:0:null,

            })
            this.data=res.data.records
        },
         // 新增
       async addnoticeList(){        
           const res:any=await add_noticeList(this.noticeForm)
           if(res.status===200){
            this.addnoticeDisplay=false      
            this.getList()
            this.noticeForm={}
        }
        },
        //修改
        noticeDisplay(row:any){
            this.editnoticeDisplay=true
            this.editnoticeForm=row
        },
        async editnoticeList(){
            const res:any=await edit_noticeList(this.editnoticeForm)
            this.editnoticeDisplay=false
            if(res.status===200){
                this.getList()
            }
        },
        // 分页
        noticeSizeChange(page:any){
            this.current=1
            this.size=page
            this.getList()

        },
        noticeCurrentChange(page:any){
            this.current=page
            this.getList()
        }
    }
})
export default noticeStore