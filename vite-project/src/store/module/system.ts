import {defineStore} from 'pinia'
import { edit_menu,add_menu,del_menu,_del_timList,_Edit_timList,_get_edit_timList,_add_timList , _get_menuList ,  _hf_timList,_stop_timList , _get_timList , edit_item , _getlog_list , _get_paraList , _getadmin_list , del_item , add_item} from '../../api/system'
import local from '../../utils/util'
import {treeDataTranslate} from '../../utils/tool'
import { get_menuList } from '../../api/menu'
import {get_adminList,get_adminroleList,add_adminList,edit_adminiList,del_adminList} from '../../api/admin' 
import { ElMessage } from 'element-plus'
const systemStore:any=defineStore('system',{
    state(){
        return{

            //管理员
            systemdata:[],
            roledata:[],
            timdata:[],
            paradata:[],
            logdata:[],


            // 管理员列表
            menuList:[],
            adminList:[],
            adminCurrent:1,
            adminSize:10,
            adminDetail:{},
            adminSearch:{},
            addadminDisplay:false,
            editadminDisplay:false,
            adminForm:{
                username: '',
                password: '',
                comfirmPassword: '',
                email: '',
                mobile: '',
                roleIdList: [],
                status: 0,
            },
            adminRoleList:[],
            addadminListId:[] as any,
            editadminForm:{
                username: '',
                password: '',
                comfirmPassword: '',
                email: '',
                mobile: '',
                roleIdList: [],
                status: 0,
            },
            addadminListAll:[] as any,




            //菜单管理
            menudata:[],
            menudataForm:[],
            menudialogVisible:false,
            menuFormData:{
                menuradio:'1',
                name:'',
                parentId:0,
                icon:'',
                url:'',
                perms:'',
                orderNum:0
            },
            EditmenudialogVisible:false,
            EditmenuFormData:{
                menuradio:'1',
                name:'',
                parentId:0,
                icon:'',
                url:'',
                perms:'',
                orderNum:0,
                menuId:0
            },
            



            //参数管理
            configdialogVisible:false,
            configAdddialogVisible:false,
            configEditdialogVisible:false,
            configDelList:[],
            configFromList:{
                paramKey:'',
                paramValue:'',
                remark:''
            },
            configEditForm:{
                paramKey:'',
                paramValue:'',
                remark:'',
                id:''
            },
            configTotal:0,
            configSearch:{
                paramKey:''
            },
            configcurrent:1,
            configsize:10,
            configtotal:0,


            //系统日志
            systemSearch:{
                username:'',
                operation:''
            },
            logcurrent:1,
            logsize:10,
            logtotal:0,
            
            
            //定时任务
            timpage:1,
            timlimit:10,
            timtotal:0,
            timAdddialogVisible:false,
            timEditdialogVisible:false,
            timsearch:{
                beanName:''
            },
            timAddForm:{
                beanName:'',
                methodName:'',
                params:'',
                cronExpression:'',
                remark:''
            },
            timEditForm:{
                beanName:'',
                methodName:'',
                params:'',
                cronExpression:'',
                remark:''
            },
            timselectAll:[],
            timEditId:'',
            adminoption: {
                search: {},
                border: true,
                align: 'center',
                addBtn: false,
                searchBtn: false,
                emptyBtn: false,
                selection: true,
                searchMenuSpan: 3,
                editBtn: false,
                delBtn: false,
                columnBtn:false,
                column: [{
                    label: '用户名',
                    prop: 'username',
                    search: true,
                    key:1,
                    hide:false
                }, {
                    label: '邮箱',
                    prop: 'email',
                    key:2,
                    hide:false
                }, {
                    label: '手机号',
                    prop: 'mobile',
                    key:3,
                    hide:false
                }, {
                    label: '创建时间',
                    prop: 'createTime',
                    key:4,
                    hide:false
                }, {
                    label: '状态',
                    prop: 'status',
                    slot: true,
                    key:5,
                    hide:false
                }
                ]
            },
            admincolumnDisplay:false,
            admincolumnValue:[] as any,
            RzdialogVisible:false,
            RzInput:'',
            RztableData:[]

        }
    },
    actions:{
        //系统日志
        async get_list(){
            const res:any=await _getlog_list({
              t:1666919667739,
              current:this.logcurrent,
              size:this.logsize
            })
            this.logtotal=res.data.total
            this.logdata=res.data.records
        },
        async searchsys(){
            const res:any=await _getlog_list({
                current:this.logcurrent,
                size:this.logsize,
                username:this.systemSearch.username,
                operation:this.systemSearch.operation
            })
            this.logtotal=res.data.total
            this.logdata=res.data.records
        },

        //参数管理
        async searchconfig(){
            const res:any=await _get_paraList({
                current:this.configcurrent,
                size:this.configsize,
                paramKey:this.configSearch.paramKey
            })
            this.paradata=res.data.records
            this.configTotal=res.data.total
        },
        async get_configlist(){
            const res:any=await _get_paraList({
              t:1666919667739,
              current:this.configcurrent,
              size:this.configsize
            })
            console.log(res)
            this.paradata=res.data.records
            this.configTotal=res.data.total
        },

        async configdelItem(){
            del_item(this.configDelList)
            this.get_configlist()
        },
        async configAddTrue(){
            const res=await add_item({
                paramKey:this.configFromList.paramKey,
                paramValue:this.configFromList.paramValue,
                remark:this.configFromList.remark
            })
            this.get_configlist()
            this.configAdddialogVisible=false
        },
        async configEditItem(){
            await edit_item({
                id:this.configEditForm.id,
                paramKey:this.configEditForm.paramKey,
                paramValue:this.configEditForm.paramValue,
                remark:this.configEditForm.remark,
            })
            this.get_configlist()
            this.configEditdialogVisible=false
        },

        //定时任务
        async get_timlist(){
            const res:any=await _get_timList({
              t:1666919667739,
              page:this.timpage,
              limit:this.timlimit,
              beanName:''
            })
            this.timdata=res.data.records
            this.timtotal=res.data.total
        },
        async timSearch(){
            const res:any=await _get_timList({
                t:1666919667739,
                page:this.timpage,
                limit:this.timlimit,
                beanName:this.timsearch.beanName
            })
            this.timdata=res.data.records
            this.timtotal=res.data.total
        },
        async timAdd(){
            await _add_timList({
                beanName:this.timAddForm.beanName,
                cronExpression:this.timAddForm.cronExpression,
                methodName:this.timAddForm.methodName,
                params:this.timAddForm.params,
                remark:this.timAddForm.remark
            })
            this.get_timlist()
            this.timAddForm={
                beanName:'',
                methodName:'',
                params:'',
                cronExpression:'',
                remark:''
            },
            this.timAdddialogVisible=false
        },
        async timEdit(){
            await _Edit_timList(this.timEditForm)
            this.timEditdialogVisible=false
            this.get_timlist()
        },
        async stopTimItem(){
            let Ids=[]
            Ids=this.timselectAll.map((item:any)=>{
                return item.jobId
            })
            await _stop_timList(Ids)
            this.get_timlist()
        },
        async stopOne(id:any){
            await _stop_timList([id])
            this.get_timlist()
        },
        async HfItem(){
            let Ids=[]
            Ids=this.timselectAll.map((item:any)=>{
                return item.jobId
            })
            await _hf_timList(Ids)
            this.get_timlist()
        },
        async HfOne(id:any){
            await _hf_timList([id])
            this.get_timlist()
        },
        async getEditList(id:any){
            const res:any=await _get_edit_timList(id)
            this.timEditForm=res.data
        },
        async delAllList(){
            let Ids=[]
            Ids=this.timselectAll.map((item:any)=>{
                return item.jobId
            })
            await _del_timList(Ids)
            this.get_timlist()
        },
        async delOne(id:any){
            await _del_timList([id])
            this.get_timlist()
        },


        //菜单数据
        async GetenuList(){
            const res:any=await _get_menuList()
            this.menudata=treeDataTranslate(res.data,'menuId','parentId')
        },
        async DelMenu(id:any){
            await del_menu(id)
            this.GetenuList()
        },
        //新增菜单
        async addMenu(){

        },
        handleSelectMenuChange(val:any){
            this.menuFormData.parentId = val[val.length - 1]
        },
        async menuAddTrue(){
            if(this.menuFormData.menuradio=='0'){
                await add_menu(
                    {
                        icon:'',
                        name:this.menuFormData.name,
                        orderNum:this.menuFormData.orderNum,
                        parentId:this.menuFormData.parentId,
                        perms:this.menuFormData.perms,
                        type:0,
                        url:this.menuFormData.url
                    }
                )
            }else if(this.menuFormData.menuradio=='1'){
                await add_menu(
                    {
                        icon:'',
                        name:this.menuFormData.name,
                        orderNum:this.menuFormData.orderNum,
                        parentId:this.menuFormData.parentId,
                        perms:this.menuFormData.perms,
                        type:1,
                        url:this.menuFormData.url
                    }
                )
            }else{
                await add_menu(
                    {
                        icon:'',
                        name:this.menuFormData.name,
                        orderNum:this.menuFormData.orderNum,
                        parentId:this.menuFormData.parentId,
                        perms:this.menuFormData.perms,
                        type:2,
                        url:this.menuFormData.url
                    }
                )
            }
            
            this.menudialogVisible=false
            this.GetenuList()
        },
        async getMenuEditList(row:any){
            this.EditmenudialogVisible=true
            console.log(row)
            if(row.type===0){
                this.EditmenuFormData={
                    menuradio:'0',
                    name:row.name,
                    parentId:row.parentId,
                    icon:row.icon,
                    url:row.url,
                    perms:row.perms,
                    orderNum:row.orderNum,
                    menuId:row.menuId
                }
            }else if(row.type===1){
                this.EditmenuFormData={
                    menuradio:'1',
                    name:row.name,
                    parentId:row.parentId,
                    icon:row.icon,
                    url:row.url,
                    perms:row.perms,
                    orderNum:row.orderNum,
                    menuId:row.menuId
                }
            }else{
                this.EditmenuFormData={
                    menuradio:'2',
                    name:row.name,
                    parentId:row.parentId,
                    icon:row.icon,
                    url:row.url,
                    perms:row.perms,
                    orderNum:row.orderNum,
                    menuId:row.menuId
                }
            }
        },
        async menuEditTrue(){
            if(this.EditmenuFormData.menuradio=='0'){
                await edit_menu({
                    icon:this.EditmenuFormData.icon,
                    name:this.EditmenuFormData.name,
                    orderNum:this.EditmenuFormData.orderNum,
                    parentId:this.EditmenuFormData.parentId,
                    perms:this.EditmenuFormData.perms,
                    type:0,
                    url:this.EditmenuFormData.url,
                    menuId:this.EditmenuFormData.menuId
                })
            }else if(this.EditmenuFormData.menuradio=='1'){
                await edit_menu({
                    icon:this.EditmenuFormData.icon,
                    name:this.EditmenuFormData.name,
                    orderNum:this.EditmenuFormData.orderNum,
                    parentId:this.EditmenuFormData.parentId,
                    perms:this.EditmenuFormData.perms,
                    type:1,
                    url:this.EditmenuFormData.url,
                    menuId:this.EditmenuFormData.menuId
                })
            }else{
                await edit_menu({
                    icon:this.EditmenuFormData.icon,
                    name:this.EditmenuFormData.name,
                    orderNum:this.EditmenuFormData.orderNum,
                    parentId:this.EditmenuFormData.parentId,
                    perms:this.EditmenuFormData.perms,
                    type:2,
                    url:this.EditmenuFormData.url,
                    menuId:this.EditmenuFormData.menuId
                })
            }

            this.EditmenudialogVisible=false
            this.GetenuList()
        },

        // 菜单管理
        admincolumnDisplayTrue(){
            this.admincolumnDisplay=true
            this.admincolumnValue = []
            this.adminoption.column.forEach((item: any) => {
                if (item.hide === false) {
                    this.admincolumnValue.push(item.key)
                }
            })
        },
        adminhandleChange(value: any, direction: any, movedKeys: any) {
            if (direction === 'left') {
                this.adminoption.column.forEach((item: any) => {
                    movedKeys.forEach((v: any) => {
                        if (item.key === v) {
                            item.hide = true
                        }
                    })
                })
            }
            else {
                this.adminoption.column.forEach((item: any) => {
                    movedKeys.forEach((v: any) => {
                        if (item.key === v) {
                            item.hide = false
                        }
                    })
                })
            }
        },
        async get_menuList(){
            const res:any=await get_menuList()
            this.menuList=res.data
        },
        // 管理员列表
        async get_adminList(){
            const res:any=await get_adminList({
                current:this.adminCurrent,
                size:this.adminSize
            })
            this.adminList=res.data.records
            this.adminDetail=res.data
            
        },
        //管理员角色数据
        async get_adminroleList(){
            const res:any=await get_adminroleList()
            this.adminRoleList=res.data
        },
        // 管理员分页
        adminCurrentChange(page: number) {
            this.adminCurrent = page
            this.get_adminList()
        },
        adminSizeChange(page: number) {
            this.adminCurrent = 1
            this.adminSize = page
            this.get_adminList()
        },
        // 管理员搜索
        async searchadminList(row:any){
            const res: any = await get_adminList({
                current: this.adminCurrent,
                size: this.adminSize,
                username: row.username
            })
            this.adminDetail = res.data
            this.adminList = res.data.records
            this.adminCurrent=1
        },
        // 管理员清空
        clearadminSearch() {
            this.adminSearch = {}
            this.get_adminList()
        },
        // 管理员新增
        addadminDisplaytrue(){
            this.addadminDisplay=true
            this.get_adminroleList()
        },
        Idpush(item:any){
            this.addadminListId.push(item.roleId)
        },
        async addadminList(){
            const res:any=await add_adminList({
                username:this.adminForm.username,
                password: this.adminForm.password,
                email: this.adminForm.email,
                mobile: this.adminForm.mobile,
                status: this.adminForm.status,
                roleIdList: [...new Set(this.addadminListId)]
            })
            if(res.status===200){
                this.addadminDisplay=false
                ElMessage({
                    type: 'success',
                    message: '操作成功',
                  })
                  this.get_adminList()
            }
        },
        // 管理员修改
        adminDisplay(row:any){
          this.editadminDisplay=true
          this.editadminForm=row
          this.get_adminroleList()
        },
        async editadminList(){
            const res:any=await edit_adminiList(this.editadminForm)
            this.editadminDisplay=false
            if(res.status===200){
                this.get_adminList()
            }
        },
        // 管理员批量删除
        selectionChange(val:any){
            this.addadminListAll=val
        },
        delAlladminList(){
            const adminListId:any=[]
            this.addadminListAll.forEach((item:any)=>{
                adminListId.push(item.userId)
            })
            del_adminList(adminListId)
            // del_adminList(this.addadminListAll)
        }
    }
})
export default systemStore