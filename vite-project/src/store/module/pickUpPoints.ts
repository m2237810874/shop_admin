import {defineStore} from 'pinia'
import {pickUpPonits_list,deleteone,add_pickList,edit_pickList,delete_many,add_addressList,add_cityList,add_areaList} from "../../api/pickUpPoints"
const pickUpPointsStore:any=defineStore('pickUpPoints',{
    state(){
        return{
           data:[],
           dialogVisible:false,
           delId:null,
           current:1,
           size:10,
           pickpage:{},
           proSearch:{

           },
        //    新增
           pickUpPointsDisplay:false,
           pickUpPointsForm:{
            addr: "",
            addrName: "",
            area: "",
            areaId: 0,
            city: "",
            cityId: 0,
            mobile: "",
            province: "",
            provinceId: 0,
           }as any,
        //    编辑
           editpickDisplay:false,
           editpickForm:{
            addr: "",
            addrName: "",
            area: "",
            areaId: 0,
            city: "",
            cityId: 0,
            mobile: "",
            province: "",
            provinceId: 0,
           },
           addrId:[],
           provinceList:[],
           cityList:[],
           areaList:[],
           pid:0,
           provinceId:0,
           cityId:0,
           areaId:0
        }
    },
    actions:{
        async getList (){
            const res:any=await pickUpPonits_list({
                t:1666954310292,
                current:this.current,
                size:this.size
            })
            this.data=res.data.records
            // 分页
            this.pickpage=res.data
          },
          async truedelone(){
            await deleteone([this.delId])
            this.getList()
        },
         //批量删除 
         async delets() {
            const id: any = []
            this.addrId.forEach((item: any) => {
                id.push(item.addrId)
            })
            const res: any = await delete_many(id)
            console.log(res);
            
            if (res.status === 200) {
                this.getList()
            }
        },
        pickChange(value: any) {
            this.addrId = value

        },
          //搜索
        async searchnoList(row:any){
            const res:any=await pickUpPonits_list({
                current:this.current,
                size:this.size,
                addrName:row.addrName,
            })
            this.data=res.data.records
        },
        async pickUpPointsDisplayTrue(){
            this.pickUpPointsDisplay=true
            const res:any=await add_addressList({
                pid:this.pid
            })
            this.provinceList=res.data
        },
        async selectProvince(val:any){
            this.provinceList.forEach((item:any)=>{
                if(item.areaName===val){
                    this.provinceId=item.areaId
                    this.pickUpPointsForm.provinceId=item.areaId
                    this.editpickForm.provinceId=item.areaId
                }
            })
            const res:any=await add_cityList({
                pid:this.provinceId
            })
            this.cityList=res.data
        },
        async selectCity(val:any){
            this.cityList.forEach((item:any)=>{
                if(item.areaName===val){
                    this.cityId=item.areaId
                    this.pickUpPointsForm.cityId=item.areaId
                    this.editpickForm.cityId=item.areaId
                }
            })
            const res:any=await add_areaList({
                pid:this.cityId
            })
            this.areaList=res.data
        },
        selectarea(val:any){
            this.areaList.forEach((item:any)=>{
                if(item.areaName===val){
                    this.areaId=item.areaId
                    this.pickUpPointsForm.areaId=item.areaId
                    this.editpickForm.areaId=item.areaId
                }
            })
        },
         // 新增
       async addpickList(){
           const res:any=await add_pickList(this.pickUpPointsForm)
           console.log(res);
           
           if(res.status===200){
            this.pickUpPointsDisplay=false
            this.getList()
            this.pickUpPointsForm={}
           }
            
        },
          //修改
        async editpick(row:any){
            this.editpickDisplay=true
            this.editpickForm=row
            const resprovince:any=await add_addressList({
                pid:row.provinceId
            })
            this.provinceList=resprovince.data
            const rescity:any=await add_cityList({
                pid:row.provinceId
            })
            this.cityList=rescity.data
            const resarea:any=await add_areaList({
                pid:row.cityId
            })
            this.areaList=resarea.data
        },
        async editpickList(){
            const res:any=await edit_pickList(this.editpickForm)
            this.editpickDisplay=false
            if(res.status===200){
                this.getList()
            }
        },
        // 分页
        pickSizeChange(page:any){
            this.current=1
            this.size=page
            this.getList()
        },
        pickCurrentChange(page:any){
            this.current=page
            this.getList()
        }
    }
})
export default pickUpPointsStore