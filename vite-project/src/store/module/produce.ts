import {defineStore} from 'pinia'
import { get_PSarea,get_specValue,_get_transport,_del_one ,_get_produce , _get_spec , _get_prodTag ,_get_listCategory} from '../../api/produce'
import {treeDataTranslate} from '../../utils/tool'
const produceStore:any=defineStore('produce',{
    state(){
        return{
            data:[],
            dialogVisible:false,
            delId:null,
            current:1,
            size:10,
            proSearch:{},
            proInfoFormData:{
                status:1
            },
            specList:[],
            specSelected:[],
            prodTagList:[],
            prodSelected:[],
            listCategory:[],
            CategorySelected:[],
            selectAll:[],
            total:0,
            addGg:false,

            TableItem:[
                {
                    ShopPrice:0.01,
                    ScPrice:0.01,
                    HaveShop:0,
                    ShopWeight:0,
                    ShopVolumn:0,
                    AddTableItem:[
                        
                    ],
                }
            ],
            
            


            Shoptextarea:'',
            TShopArr:[],
            PStrue:false,
            YF:'',
            YunFeiList:[],
            AddGg:false,
            GgForm:{
                GgName:'',
                GgValue:''
            },
            SelectSpec:'',
            SpecValueList:[],
            SpecValue:'',
            iptDisabled:false,
            PSList:[]

        }
    },
    actions:{
        async getlist(){
            const res:any=await _get_produce({
              t:1666867640113,
              current:this.current,
              size:this.size
            })
            this.data=res.data.records
            this.total=res.data.total
        },
        async truedelone (){
            await _del_one([this.delId])
            this.getlist()
        },
        async searchproList(row:any){
            console.log(row)
            if(row.status==='未上架'){
                const res:any=await _get_produce({
                    current:this.current,
                    size:this.size,
                    prodName:row.prodName,
                    status:0
                })
                this.data=res.data.records
            }else if(row.status==='上架'){
                const res:any=await _get_produce({
                    current:this.current,
                    size:this.size,
                    prodName:row.prodName,
                    status:1
                })
                this.data=res.data.records
            }else{
                const res:any=await _get_produce({
                    current:this.current,
                    size:this.size,
                    prodName:row.prodName
                })
                this.data=res.data.records
            }
        },
        async getspecList(){
            const res:any=await _get_spec({
                t:1667194678656
            })
            this.specList=res.data
            console.log(this.specList)
        },
        async getprodTagList(){
            const res:any=await _get_prodTag({
                t:1667194678656
            })
            this.prodTagList=res.data
        },
        async getlistCategory(){
            const res:any=await _get_listCategory({
                t:1667195336669
            })
            this.listCategory=treeDataTranslate(res.data,'categoryId', 'parentId')
            console.log(this.listCategory)
        },
        async del_all(){
            let Ids:any=[]
            this.selectAll.forEach((item:any,index)=>{
                Ids.push(item.prodId as number)
            })
            await _del_one(Ids)
            this.getlist()
        },
        SelectShop(value:any){
            console.log(this.TShopArr)
            this.PStrue=false
            this.TShopArr.forEach((item:any)=>{
                if(item==='Option 1'){
                    this.PStrue=true
                }
            })
            console.log(this.PStrue)
        },
        //获取运费设置
        async getTransport(){
            const res:any=await _get_transport()
            this.YunFeiList=res.data
            console.log(this.YunFeiList)
        },
        AltertAddGg(){
            console.log(111)
            this.AddGg=!this.AddGg
        },
        TFBtn(){
            this.iptDisabled=!this.iptDisabled
        },
        async YFchange(val:any){
            const res:any=await get_PSarea(val)
            this.PSList=res.data
            console.log(this.PSList)
        },
        async GetSpaceValue(value:any){
            const res:any=await get_specValue(value)
            this.SpecValue=''
            this.SpecValueList=res.data
        },
        getSpecValue(){
            let SelectSpec=''
            this.specList.forEach((item:any)=>{
                if(item.propId===this.SelectSpec){
                    SelectSpec=item.propName
                }
            })
            
            console.log(SelectSpec)
            console.log(this.SpecValue)
        }
    }
})
export default produceStore