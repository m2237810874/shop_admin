import useproductStore from './module/product'
import useloginStore from './module/login'
import usenoticeStore from './module/notice'
import usetoSearchStore from "./module/toSearch"
import useswiperStore from "./module/swiper"
import usefeightStore from "./module/feight"
import usepickUpPointsStore from "./module/pickUpPoints"
import userolestore from "./module/role"
import useproduceStore from './module/produce'
import usesystemStore from './module/system'
import useOrderStore from './module/order'
export default function useStore() {
    return {
        login: useloginStore(),
        product: useproductStore(),
        notice: usenoticeStore(),
        toSearch: usetoSearchStore(),
        swiper: useswiperStore(),
        feight: usefeightStore(),
        role: userolestore(),
        pickUpPoints: usepickUpPointsStore(),
        produce:useproduceStore(),
        system:usesystemStore(),
        orderstore:useOrderStore()
    }
}