// 登录
// 表单的验证规则对象
export const rulesConfig = {
    // 验证用户名是否合法
    username: [
        { required: true, message: '请输入用户名', trigger: 'blur' },
        { min: 2, max: 10, message: '长度不对', trigger: 'blur' }
    ],
    // 验证密码是否合法
    pwd: [{ required: true, message: '请输入密码', trigger: 'blur' }],
    // 验证码
    code: [{ required: true, message: '请输入验证码', trigger: 'blur' }]
}