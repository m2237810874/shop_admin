export interface memberType {
  birthDate: null;
  loginPassword: null;
  modifyTime: string;
  nickName: string;
  payPassword: null;
  pic: string | null;
  realName: null;
  score: null;
  sex: string;
  status: number;
  userId: string;
  userLastip: null;
  userLasttime: null;
  userMail: null;
  userMemo: null;
  userMobile: null;
  userRegip: null;
  userRegtime: string;
}
export interface editMemberType {
  nickName: string;
  status: number;
  pic: string | null;
  userId: string;
}
export interface memberResType {
  current: number;
  pages: number;
  records: memberType[];
  searchCount: boolean;
  size: number;
  total: number;
}
