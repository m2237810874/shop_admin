import request from "../utils";
export const get_classList = () => {
    return request.get({
        url: '/api/prod/category/table',
        method: 'GET'
    })
}
export const del_classList = (id: string) => {
    return request.delete({
        url: `/api/prod/category/${id}`,
        method: 'DELETE'
    })
}
export const add_classList = (data: any) => {
    return request.post({
        url: '/api/prod/category',
        method: 'POST',
        data
    })
}
export const edit_classList = (data: any) => {
    return request.put({
        url: '/api/prod/category',
        method: 'PUT',
        data
    })
}