import request  from "../utils";

export const feightList=(params:any)=>{
    return request.get({
        url:'/api/shop/transport/page',
        method:'GET',
        params
    })
}

export const deleteone=(params:any)=>{
    return request.delete({
        url:'/api/shop/transport',
        method:"DELETE",
        data:params
    })
}

export const delete_many=(params:any)=>{
    return request.delete({
        url:'/api/shop/transport',
        method:"DELETE",
        data:params
    })
}
 
export const add_feightList=(data:any)=>{
    return request.post({
        url:'/api/shop/transport',
        method:"POST",
        data
    })
}
 

export const edit_feightList=(data:any)=>{
    return request.put({
        url:'/api/shop/transport',
        method:"PUT",
        data
    })
}
 