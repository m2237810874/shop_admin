import request  from "../utils";

export const pickUpPonits_list=(params:any)=>{
    return request.get({
        url:'api/shop/pickAddr/page',
        method:'GET',
        params
    })
}
export const deleteone=(params:any)=>{
    return request.delete({
          url:'/api/shop/pickAddr',
          method:'DELETE',
          data:params
    })
}
export const delete_many=(params:any)=>{
    return request.delete({
          url:'/api/shop/pickAddr',
          method:'DELETE',
          data:params
    })
}
// 新增
export const add_pickList=(data:any)=>{
    return request.post({
        url:'/api/shop/pickAddr',
        method:"POST",
        data
    })
}
// 编辑
export const edit_pickList=(data:any)=>{
    return request.put({
        url:'/api/shop/pickAddr',
        method:"PUT",
        data
    })
}

//地址
export const add_addressList=(data:any)=>{
    return request.get({
        url:'/api/admin/area/listByPid',
        method:"GET",
        params:data
    })
}
export const add_cityList=(data:any)=>{
    return request.get({
        url:'/api/admin/area/listByPid',
        method:"GET",
        params:data
    })
}
export const add_areaList=(data:any)=>{
    return request.get({
        url:'/api/admin/area/listByPid',
        method:"GET",
        params:data
    })
}
