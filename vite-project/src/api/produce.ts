import request from '../utils'

export const _get_produce=(params:any)=>{
    return request.get({
        url:'/api/prod/prod/page',
        method:'GET',
        params
    })
}

export const _del_one=(params:any)=>{
    return request.delete({
        url:'/api/prod/prod',
        method:'DELETE',
        data:params
    })
}

export const _get_spec=(params:any)=>{
    return request.get({
        url:'/api/prod/spec/list',
        method:'GET',
        params
    })
}

export const _get_prodTag=(params:any)=>{
    return request.get({
        url:'/api/prod/prodTag/listTagList',
        method:'GET',
        params
    })
}


//产品分类
export const _get_listCategory=(params:any)=>{
    return request.get({
        url:'/api/prod/category/listCategory',
        method:'GET',
        params
    })
}

//运费设置
// /shop/transport/list
export const _get_transport=()=>{
    return request.get({
        url:'/api/shop/transport/list',
        method:'GET'
    })
}


//获取规格值
// /prod/spec/listSpecValue
export const get_specValue=(id:any)=>{
    return request.get({
        url:`/api/prod/spec/listSpecValue/${id}`,
        method:'GET'
    })
}

//  /shop/transport/info
//配送区域
export const get_PSarea=(id:any)=>{
    return request.get({
        url:`/api/shop/transport/info/${id}`,
        method:'GET'
    })
}