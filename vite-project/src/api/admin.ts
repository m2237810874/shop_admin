import request from "../utils";
export const get_adminList=(data:any)=>{
    return request.get({
        url:'/api/sys/user/page',
        method:'GET',
        params:data
    })
}
export const get_adminroleList=()=>{
    return request.get({
        url:'/api/sys/role/list',
        method:'GET'
    })
}
export const add_adminList=(data:any)=>{
    return request.post({
        url:'/api/sys/user',
        method:'POST',
        data
    })
}
export const del_adminList=(id:string)=>{
    return request.delete({
        url:'/api/sys/user',
        method:"DELETE",
        data:[id]
    })
}
export const edit_adminiList=(data:any)=>{
    return request.put({
        url:'/api/sys/user',
        method:'PUT',
        data
    })
}