import request from '../utils'
import { ResponseData, LoginResponseData } from '../types/loginType'
// 用户登录请求接口
export const _login = (params: any) => {
    return request.post<ResponseData<LoginResponseData>>({
        url:`/api/login?grant_type=admin`,
        method:'POST',
        data:params
    })
}

export const _get_userinfo=()=>{
    return request.get({
        url:'/api/sys/user/info',
        method:'GET'
    })
}

export const _get_menulist=()=>{
    return request.get({
        url:'/api/sys/menu/nav',
        method:'GET'
    })
}

export const changePwd=(data:any)=>{
    return request.post({
        url:'/api/sys/user/password',
        method:'POST',
        data
    })
}