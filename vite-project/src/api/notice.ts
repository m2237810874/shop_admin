import request from '../utils/index'
//渲染
export const get_noticeList=(params:any)=>{
    return request.get({
        url:"/api/shop/notice/page",
        method:'GET',
        params
    })
}
// 删除
export const deleteone=(params:any)=>{
    return request.delete({
        url:`/api/shop/notice/${params}`,
        method:"DELETE"
    })
}
// 新增
export const add_noticeList=(data:any)=>{
    return request.post({
        url:'/api/shop/notice',
        method:"POST",
        data
    })
}


// 编辑
export const edit_noticeList=(data:any)=>{
    return request.put({
        url:'/api/shop/notice',
        method:"PUT",
        data
    })
}
