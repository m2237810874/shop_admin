import request from '../utils'
import { tableType } from "../types";

//管理员列表
//获取管理员列表数据
export const _getadmin_list = (params: any) => {
    return request.get({
        url: '/api/sys/user/page',
        method: 'GET',
        data: params
    })
}
'----------------------------------------------------------------------------------------------------------------'

//角色管理
//获取角色管理
export const _getrole_list = (params: any) => {
    return request.get({
        url: '/api/sys/role/page',
        method: 'GET',
        data: params
    })
}

'----------------------------------------------------------------------------------------------------------------'

//定时任务
//获取定时任务列表数据
export const _get_timList = (params: any) => {
    return request.get({
        url: '/api/sys/schedule/page',
        method: 'GET',
        params
    })
}

//新增  /sys/schedule
export const _add_timList=(data:any)=>{
    return request.post({
        url:'/api/sys/schedule',
        method:'POST',
        data
    })
}

'----------------------------------------------------------------------------------------------------------------'

//获取编辑数据 /sys/schedule/info/69
export const _get_edit_timList=(id:any)=>{
    return request.get({
        url:`/api/sys/schedule/info/${id}`,
        method:'GET'
    })
}


//暂停
///sys/schedule/pause
export const _stop_timList=(data:any)=>{
    return request.post({
        url:'/api/sys/schedule/pause',
        method:'POST',
        data
    })
}

//恢复
// /sys/schedule/resume 
export const _hf_timList=(data:any)=>{
    return request.post({
        url:'/api/sys/schedule/resume ',
        method:'POST',
        data
    })
}

//修改
// /sys/schedule
export const _Edit_timList=(data:any)=>{
    return request.put({
        url:'/api/sys/schedule',
        method:'PUT',
        data
    })
}


export const _del_timList=(data:any)=>{
    return request.delete({
        url:'/api/sys/schedule',
        method:'DELETE',
        data
    })
}

'----------------------------------------------------------------------------------------------------------------'

//参数管理
//获取参数管理页面数据
export const _get_paraList = (params: any) => {
    return request.get({
        url:'/api/sys/config/page',
        method:'GET',
        params
    })
}

export const del_item=(data:any)=>{
    return request.delete({
        url:'/api/sys/config',
        method:'DELETE',
        data:data
    })
}

export const add_item=(data:any)=>{
    return request.post({
        url:'/api/sys/config',
        method:'POST',
        data
    })
}

export const edit_item=(data:any)=>{
    return request.put({
        url:'/api/sys/config',
        method:'PUT',
        data
    })
}

'----------------------------------------------------------------------------------------------------------------'

//获取系统日志数据
export const _getlog_list = (params: any) => {
    return request.get({
        url: '/api/sys/log/page',
        method: 'GET',
        data: params
    })
}


// 地址管理
export const getAddress = (params: any) =>
 request.get({
    url: "/api/admin/area/list",
    method: "GET",
    params,
  });
  export const getAddressInfo = (id: number) =>
  request.get({
    url: `/api/admin/area/info/${id}`,
    method: "GET",
  });
export const editAddress = (data: any) =>
 request.put({
    url: "/api/admin/area",
    method: "PUT",
    data,
  });
  export const delAddress = (id: number) =>
  request.delete({
    url: `/api/admin/area/${id}`,
    method: "DELETE",
  });
export const saveAddress = (data: any) =>
  request.post({
    url: "/api/admin/area",
    method: "POST",
    data,
  });


  // 会员管理
interface getMemberReqType extends tableType {
  nickName?: string;
  status?: number;
}
interface editMemberType {
  nickName: string;
  status: number;
  userId: string;
}
export const getMember = (params: getMemberReqType) =>
  request.get({
    url: "/api/admin/user/page",
    method: "GET",
    params,
  });
export const editMember = (data: editMemberType) =>
  request.put({
    url: "/api/admin/user",
    method: "PUT",
    data,
  });

'----------------------------------------------------------------------------------------------------------------'

//获取菜单数据
export const _get_menuList=()=>{
    return request.get({
        url:'/api/sys/menu/table',
        method:'GET'
    })
}

//删除
export const del_menu=(id:any)=>{
    return request.delete({
        url:`/api/sys/menu/${id}`,
        method:'DELETE'
    })
}

//新建
export const add_menu=(data:any)=>{
    return request.post({
        url:'/api/sys/menu',
        method:'POST',
        data
    })
}

//获取编辑数据
export const get_menu_edit=(id:any)=>{
    return request.get({
        url:`/api/sys/menu/${id}`,
        method:'GET'
    })
}

//编辑
export const edit_menu=(data:any)=>{
    return request.put({
        url:`/api/sys/menu`,
        method:'PUT',
        data
    })
}
