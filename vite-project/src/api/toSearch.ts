import request from "../utils"

export const toSearchList=(params:any)=>{
   return request.get({
         url:'/api/admin/hotSearch/page',
         method:'GET',
         params

   })
}
//删除
export const delete_one=(params:any)=>{
      return request.delete({
            url:'/api/admin/hotSearch',
            method:'DELETE',
            data:params
      })
}
//批量删除
export const delete_many=(params:any)=>{
      return request.delete({
            url:'/api/admin/hotSearch',
            method:'DELETE',
            data:params
      })
}
// 新增
export const add_toSearchList=(data:any)=>{
      return request.post({
          url:'/api/admin/hotSearch',
          method:"POST",
          data
      })
  }

// 编辑
export const edit_toSearchList=(data:any)=>{
      return request.put({
          url:'/api/admin/hotSearch',
          method:"PUT",
          data
      })
  }
