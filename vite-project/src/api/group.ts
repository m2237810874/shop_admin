import request from '../utils/index'


export const get_groupList=(data:any)=>{
    return request.get({
        url:'/api/prod/prodTag/page',
        method:'GET',
        params:data
    })
}
export const add_groupList=(data:any)=>{
    return request.post({
        url:'/api/prod/prodTag',
        method:'POST',
        data
    })
}
export const edit_groupList=(data:any)=>{
    return request.put({
        url:'/api/prod/prodTag',
        method:'PUT',
        data
    })
}
export const del_groupList=(id:any)=>{
    return request.delete({
        url:`/api/prod/prodTag/${id}`,
        method:'DELETE',
    })
}