import request from "../utils";

export const swiperList=(params:any)=>{
    return request.get({
        url:'/api/admin/indexImg/page',
        method:'GET',
        params
    })
}
// 删除
export const deleteone=(params:any)=>{
    return request.delete({
        url:'/api/admin/indexImg',
        method:"DELETE",
        data:params
    })
}
// 批量删除
export const delete_many=(params:any)=>{
    return request.delete({
        url:'/api/admin/indexImg',
        method:"DELETE",
        data:params
    })
}
// 新增
export const add_swiperList=(data:any)=>{
    return request.post({
        url:'/api/admin/indexImg',
        method:"POST",
        data
    })
}

// 编辑
export const edit_swiperList=(data:any)=>{
    return request.put({
        url:'/api/admin/indexImg',
        method:"PUT",
        data
    })
}