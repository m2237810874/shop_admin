import request from '../utils/index'


export const get_roleList=(data:any)=>{
    return request.get({
        url:'/api/sys/role/page',
        method:'GET',
        params:data
    })
}
export const get_navList=()=>{
    return request.get({
        url:'/api/sys/menu/table',
        method:'GET'
    })
}
export const del_roleList=(id:any)=>{
    return request.delete({
        url:'/api/sys/role',
        method:'DELETE',
        data:Array.isArray(id)?[...id]:[id]
    })
}
export const add_roleList=(data:any)=>{
    return request.post({
        url:'/api/sys/role',
        method:'POST',
        data
    })
}
export const get_editFormList=(roleId:any)=>{
    return request.get({
        url:`/api/sys/role/info/${roleId}`,
        method:'GET'
    })
}
export const edit_roleList=(data:any)=>{
    return request.put({
        url:'/api/sys/role',
        method:'PUT',
        data
    })
}