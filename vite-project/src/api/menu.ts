// 菜单管理
import request from "../utils";
export const get_menuList=()=>{
    return request.get({
        url:'/api/sys/menu/table',
        method:'GET'
    })
}